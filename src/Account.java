import java.io.Serializable;

import com.kowalski7cc.botclient.types.User;

/**
 * Object that rappresents a user with some configurations
 *
 */
public class Account implements Serializable {

	private static final long serialVersionUID = -4215034483162992028L;
	private User user;
	private boolean administrator;
	private boolean ignored;
	private String lastCommand;

	/**
	 * Creates a new account containing options for the user
	 * @param user
	 */
	public Account(User user) {
		this.user = user;
		administrator = false;
		ignored = false;
		lastCommand = null;
	}


	/**
	 * @return if user is an administrator
	 */
	public boolean isAdministrator() {
		return administrator;
	}


	/**
	 * @param administrator set if user is an administrator
	 */
	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}


	/**
	 * @return if user is ignored
	 */
	public boolean isIgnored() {
		return ignored;
	}


	/**
	 * @param ignored set if user is ignored
	 */
	public void setIgnored(boolean ignored) {
		this.ignored = ignored;
	}


	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}


	/**
	 * @return the lastCommand
	 */
	public String getLastCommand() {
		return lastCommand;
	}


	/**
	 * Used to store last user's command in case of multi-message commands
	 * @param lastCommand the lastCommand to set
	 */
	public void setLastCommand(String lastCommand) {
		if(lastCommand==null||lastCommand.equals(""))
			this.lastCommand=null;
		else
			this.lastCommand = lastCommand;
	}


}
