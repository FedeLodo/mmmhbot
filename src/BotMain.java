import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

import com.kowalski7cc.botclient.BotClient;
import com.kowalski7cc.botclient.types.Message;
import com.kowalski7cc.botclient.types.User;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

public class BotMain {

	private static final Logger LOGGER = Logger.getLogger("");
	private static final File configFile = new File("config.json");
	private List<Long> admins;
	private BotClient botClient;
	
	public static void main(String[] args) {
		LOGGER.info("Starting MhhBot");
		BotMain botMain = new BotMain();
		botMain.setUp(configFile);
		botMain.start();
	}
	
	/**
	 * 
	 */
	public BotMain() {
		botClient = null;
		admins = null;
	}
	
	public void setUp(File config) {
		StringBuilder stringBuilder = new StringBuilder();
		try (Scanner scanner = new Scanner(config)){
			scanner.forEachRemaining(s -> stringBuilder.append(s));
		} catch (FileNotFoundException e) {
			LOGGER.severe("Can't load configuration file: \n" + config.getAbsolutePath() + "\n" + e.getMessage());
			System.exit(1);
		}
		try {
			JSONObject rootConfig = new JSONObject(stringBuilder.toString());
			if(rootConfig.has("api_key")){
				botClient = new BotClient(rootConfig.getString("api_key"));
				LOGGER.info("Telegram token is now: "
						+ rootConfig.getString("api_key").substring(0,9) + "****************-******");
				if(rootConfig.has("admins")) {
					JSONArray jsonAdmins = rootConfig.getJSONArray("admins");
					admins = new ArrayList<Long>();
					for(int i=0; i<jsonAdmins.length(); i++) {
						admins.add(jsonAdmins.getLong(i));
					}
				}
			} else {
				LOGGER.severe("Can't find key \"api_key\" in " + config.getPath());
			}
		} catch (JSONException e) {
			LOGGER.severe("Invalid configuration:" + e.getMessage());
		}
	}
	
	public void start() {
		Thread.currentThread().setName("Message handler");
		if(botClient.isValid()){
			LOGGER.info("Connected succesfully to telegram bot platform");
		} else {
			LOGGER.severe("Can't connect to telegram bot platform.\n"
					+ "Check your internet connection and the API key.");
		}
		User me = botClient.getMe();
		LOGGER.info("Current bot ID: " + me.getId()
				+ ", Current Username: " + me.getUsername());
		AccountManager accountManager = new AccountManager();
		LOGGER.info("Account manager initialized.");
		botClient.getReciver().startPolling();
		LOGGER.info("Poller service started.");
		MessageHandler messageHandler = new MessageHandler(botClient, accountManager);
		messageHandler.setDefaultAdmins(admins);
		while(botClient.getReciver().isPollingRunning()){
			if(!botClient.getReciver().getIncomingQueue().isEmpty()) {
				Message message = botClient.getReciver().getIncomingQueue().poll();
				messageHandler.handle(message);
			}
		}
	}
	
}
			