import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.kowalski7cc.botclient.types.User;

/**
 * Object that manages multiple accounta
 *
 */
public class AccountManager {

	private List<Account> accounts;

	public AccountManager() {
		accounts = new ArrayList<Account>();
	}

	public Account newAccount(User user) {
		Account account = new Account(user);
		accounts.add(account);
		return account;
	}

	public Account getAccount(User user) {
		if(user==null)
			throw new IllegalArgumentException("user can't be null");
		return accounts.stream().filter(a -> a.getUser().equals(user)).findFirst().orElse(null);
	}

	public Account getAccount(long id) {
		return accounts.stream().filter(a -> a.getUser().getId()==id).findFirst().orElse(null);
	}

	public Account getAccount(String username) {
		if(username==null)
			throw new IllegalArgumentException("username can't be null");
		return accounts.stream().filter(a -> a.getUser().getUsername()!=null)
				.filter(a -> a.getUser().getUsername().equals(username)).findFirst().orElse(null);
	}

	public boolean removeAccount(Account account) {
		if(accounts.contains(account)) {
			accounts.remove(account);
			return true;
		} else {
			return false;
		}
	}

	public boolean removeAccount(User user) {
		Account account = getAccount(user);
		return account == null ? false : removeAccount(account);
	}

	public boolean removeAccount(long id) {
		Account account = getAccount(id);
		return account == null ? false : removeAccount(account);
	}

	public boolean removeAccount(String username) {
		Account account = getAccount(username);
		return account == null ? false : removeAccount(username);
	}
	
	public void saveToFile(File file) throws IOException {
		if(!file.exists())
			file.createNewFile();
		FileOutputStream out = new FileOutputStream(file);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(out);
		for(Account account : accounts)
			objectOutputStream.writeObject(account);
		objectOutputStream.close();
		out.close();
	}
	
	public void loadFromFile(File file) throws IOException, ClassNotFoundException {
		if(!file.exists())
			throw new FileNotFoundException(file.getName());
		accounts = new ArrayList<Account>();
		FileInputStream in = new FileInputStream(file);
		ObjectInputStream objectInputStream = new ObjectInputStream(in);
		Object buffer = null;
		do {
			buffer = objectInputStream.readObject();
			if((buffer!=null)&&(buffer instanceof Account))
				accounts.add((Account)buffer);
		} while(buffer!=null);
		objectInputStream.close();
		in.close();
	}
	
}
